package PPTLS;

import java.util.ArrayList;

public class test {

	public static void main(String[] args) {
		//Creo un juego nuevo
		Juego juego = new Juego();
		//Prueba efectiva
		juego.agregarElemento("Piedra");
		juego.agregarElemento("Papel");
		juego.agregarElemento("Tijera");

		juego.agregarRegla("Piedra le gana a Tijera");
		juego.agregarRegla("Papel le gana a Piedra");
		juego.agregarRegla("Tijera le gana a Papel");
		

		System.out.println(juego.jugar("Jugador 1 elije Piedra", "Jugador 2 elije Tijera"));
		System.out.println(juego.jugar("Jugador 1 elije Piedra", "Jugador 2 elije Piedra"));
		System.out.println(juego.jugar("Jugador 1 elije Piedra", "Jugador 2 elije Papel"));
		
//		//Prueba de grupo obligatoria, ambos juegos con el mismo TAD
//		juego.agregarElemento("Piedra");
//		juego.agregarElemento("Papel");
//		juego.agregarElemento("Tijera");
//		juego.agregarElemento("lagarto");
//		juego.agregarElemento("spock");
//		juego.agregarElemento("Tijera");
//		
//		juego.agregarRegla("papel le gana a piedra");
//		juego.agregarRegla("papel le gana a spock");
//		juego.agregarRegla("piedra le gana a tijera");
//		juego.agregarRegla("piedra le gana a lagarto");
//		juego.agregarRegla("spock le gana a piedra");
//		juego.agregarRegla("spock le gana a tijera");
//		juego.agregarRegla("lagarto le gana a spock");
//		juego.agregarRegla("lagarto le gana a papel");
//		juego.agregarRegla("tijera le gana a papel");
//		juego.agregarRegla("tijera le gana a lagarto");
//		
//		System.out.println(juego.jugar("jugador 1 elije lagarto", "jugador 2 elije piedra"));
//		System.out.println(juego.jugar("jugador 1 elije tijera", "jugador 2 elije papel"));
		
//		//Prueba 1:no se puede jugar un elemento que no exista, en este caso es TIJERA.
//		juego.agregarElemento("Piedra");
//		juego.agregarElemento("Papel");
//
//		juego.agregarRegla("Piedra le gana a Tijera");
//		juego.agregarRegla("Papel le gana a Piedra");
//		juego.agregarRegla("Tijera le gana a Papel");
//
//		System.out.println(juego.jugar("Jugador 1 elije Piedra","Jugador 2 elije Tijera" ));
		
//		//Prueba 2:En este caso PAPEL no tiene fortalezas definidas
//		juego.agregarElemento("Piedra");
//		juego.agregarElemento("Papel");
//		juego.agregarElemento("Tijera");
//
//		juego.agregarRegla("Piedra le gana a Tijera");
//		juego.agregarRegla("Tijera le gana a Papel");
//
//		System.out.println(juego.jugar("Jugador 1 elije Piedra","Jugador 2 elije Tijera" ));
		
//		//Prueba 3:En este caso se est�n agregando fortalezas entre dos elementos para que se ganen entre s�
//		juego.agregarElemento("Piedra");
//		juego.agregarElemento("Papel");
//		juego.agregarElemento("Tijera");
//
//		juego.agregarRegla("Piedra le gana a Tijera");
//		juego.agregarRegla("Papel le gana a Piedra");
//		juego.agregarRegla("Tijera le gana a Papel");
//		juego.agregarRegla("Papel le gana a Tijera");
//
//		System.out.println(juego.jugar("Jugador 1 elije Piedra","Jugador 2 elije Tijera" ));
		
//		//Prueba 4:Un elemento no puede ganarse a si mismo, esa regla no deberia existir
//		juego.agregarElemento("Piedra");
//		juego.agregarElemento("Papel");
//		juego.agregarElemento("Tijera");
//
//		juego.agregarRegla("Piedra le gana a Tijera");
//		juego.agregarRegla("Papel le gana a Piedra");
//		juego.agregarRegla("Tijera le gana a Papel");
//		juego.agregarRegla("Papel le gana a Papel");
//
//		System.out.println(juego.jugar("Jugador 1 elije Piedra","Jugador 2 elije Tijera" ));

	}
}