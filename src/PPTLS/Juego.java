package PPTLS;

import java.util.ArrayList;

public class Juego {
	ArrayList<Elemento> elementos;

	Juego() {
		elementos = new ArrayList<Elemento>();
	}

	boolean existeElemento(ArrayList<Elemento> lista, String buscado) {
		for (int i = 0; i < lista.size(); i++) {
			if (lista.get(i).nombre.equals(buscado.toUpperCase())) {
				return true;
			}
		}
		return false;

	}

	boolean existeFortaleza(ArrayList<Elemento> lista, String buscado) {
		return !(lista.get(posicionDeElem(lista, buscado.toUpperCase())).fuerteContra.isEmpty());
	}

	void agregarElemento(String elemento) {
		if (existeElemento(elementos, elemento.toUpperCase())) {
			return;
		} else {
			Elemento nuevoElemento = new Elemento(elemento.toUpperCase());
			elementos.add(nuevoElemento);
		}
	}

	void agregarRegla(String original) {
		String elementoGanador = auxiliares.primeraPalabra(original);
		String elementoPerdedor = auxiliares.ultimaPalabra(original);
		if (!existeElemento(elementos, elementoGanador.toUpperCase())
				&& !existeElemento(elementos, elementoPerdedor.toUpperCase())) {
			throw new RuntimeException("Ambos elementos ingresados no existen en la lista de elementos!");
		}
		if (!existeElemento(elementos, elementoGanador.toUpperCase())) {
			throw new RuntimeException("El elemento " + elementoGanador + " no existe!");
		}
		if (!existeElemento(elementos, elementoPerdedor.toUpperCase())) {
			throw new RuntimeException("El elemento " + elementoPerdedor + " no existe!");
		}
		if (elementoGanador.equals(elementoPerdedor)) {
			throw new RuntimeException("Un elemento no pude ganarse a el mismo");
		}
		for (int i = 0; i < elementos.size(); i++) {
			if (elementos.get(i).nombre.equals(elementoGanador.toUpperCase())) {
				elementos.get(i).fuerteContra.add(elementoPerdedor.toUpperCase());
			}
			if (elementos.get(posicionDeElem(elementos, elementoPerdedor.toUpperCase())).fuerteContra
					.contains(elementoGanador.toUpperCase()))
				throw new RuntimeException(
						"Contradiccion: no se puede agregar una regla en la que ambos elementos se ganen entre s�");
		}
	}

	Integer posicionDeElem(ArrayList<Elemento> lista, String buscado) {
		// Asumo que el elemento est� en la lista
		for (int i = 0; i < lista.size(); i++) {
			if (lista.get(i).nombre.equals(buscado.toUpperCase())) {
				return i;
			}
		}
		return 0;

	}

	Integer jugar(String jugada1, String jugada2) { //0=empate 1=gana jugador1 2=gana jugador2
		jugada1 = auxiliares.ultimaPalabra(jugada1);
		jugada2 = auxiliares.ultimaPalabra(jugada2);
		if (!existeElemento(elementos, jugada1.toUpperCase()) && !existeElemento(elementos, jugada2.toUpperCase())) {
			throw new RuntimeException("Los elementos elegidos por ambos jugadores no existen!");
		}
		if (!existeElemento(elementos, jugada1.toUpperCase())) {
			throw new RuntimeException("El elemento elegido por el jugador 1 no existe!");
		}
		if (!existeElemento(elementos, jugada2.toUpperCase())) {
			throw new RuntimeException("El elemento elegido por el jugador 2 no existe!");
		}
		if (!existeFortaleza(elementos, jugada1.toUpperCase()) && !existeFortaleza(elementos, jugada2.toUpperCase())) {
			throw new RuntimeException("Los elementos elegidos por ambos jugadores no tienen fortalezas definidas!");
		}
		if (!existeFortaleza(elementos, jugada1.toUpperCase())) {
			throw new RuntimeException("El elemento elegido por el jugador 1 no tiene fortalezas definidas!");
		}
		if (!existeFortaleza(elementos, jugada2.toUpperCase())) {
			throw new RuntimeException("El elemento elegido por el jugador 2 no tiene fortalezas definidas!");
		}
		for (int i = 0; i < elementos.size(); i++) {
			if(elementos.get(i).fuerteContra.isEmpty()){
				throw new RuntimeException("No hay fortalezas definidas para "+elementos.get(i).nombre);
			}
		}

		if (jugada1.toUpperCase().equals(jugada2.toUpperCase())) {
			return 0;
		}
		if (elementos.get(posicionDeElem(elementos, jugada1.toUpperCase())).fuerteContra
				.contains(jugada2.toUpperCase())) {
			return 1;
		} else {
			return 2;
		}

	}
}
