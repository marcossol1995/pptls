package PPTLS;

public class auxiliares {
	public static String primeraPalabra(String original) {
		original.trim();
		String nuevo = "";
		for (int i = 0; i < original.indexOf(" "); i++) {
			nuevo = nuevo + original.charAt(i);
		}
		return nuevo.toUpperCase();
	}

	public static String ultimaPalabra(String original) {
		original.trim();
		String nuevo = "";
		for (int i = original.lastIndexOf(" ") + 1; i < original.length(); i++) {
			nuevo = nuevo + original.charAt(i);
		}
		return nuevo.toUpperCase();
	}
	

}
